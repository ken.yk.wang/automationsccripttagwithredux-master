import { combineReducers } from 'redux';
import app from './appReducer';
import home from './pages/home/homeReducer';
import table from './pages/table/frontendTableReducer';
import chatroom from './pages/chatroom/ChatRoomReducer';

const multiReducer = combineReducers({
	app,
	home,
	table,
	chatroom,
});

export default multiReducer;
