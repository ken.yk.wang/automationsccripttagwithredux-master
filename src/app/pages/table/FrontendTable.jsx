import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Table, Divider, Form } from 'antd';
import CollectionCreateForm from './CollectionCreateForm';

import {
	handleCancel,
	handleForm,
	showModal,
	removeTodo,
	getData,
} from './frontendTableAction';

const FrontendTable = () => {
	const table = useSelector(state => state.table);
	const { todos, visible } = table;
	const { Column } = Table;
	const [form] = Form.useForm();
	const dispatch = useDispatch();

	useEffect(() => {
		getData(dispatch);
	}, []);

	return (
		<div className="Table">
			<CollectionCreateForm
				form={form}
				visible={visible}
				onCancel={() => handleCancel(dispatch)}
				onForm={() => handleForm(dispatch, table, form)}
			/>
			<div className="Table__button">
				<Button type="primary" onClick={() => showModal(dispatch, -1)}>
					add item
				</Button>
			</div>
			<Table dataSource={todos} pagination={{ pageSize: 8 }}>
				<Column title="OrderId" dataIndex="orderId" key="orderId" />
				<Column
					title="Calss Name"
					dataIndex="calssName"
					key="calssName"
				/>
				<Column title="Tag Type" dataIndex="tagType" key="tagType" />
				<Column
					title="Modified Time"
					dataIndex="modifiedTime"
					key="modifiedTime"
				/>
				<Column
					title="Action"
					key="action"
					render={(text, record, dataIndex) => (
						<span>
							<Button
								type="primary"
								onClick={() => showModal(dispatch, dataIndex)}
							>
								修改
							</Button>
							<Divider type="vertical" />
							<Button
								onClick={() =>
									removeTodo(dispatch, record.orderId)
								}
								type="primary"
							>
								刪除
							</Button>
						</span>
					)}
				/>
			</Table>
		</div>
	);
};

export default FrontendTable;
