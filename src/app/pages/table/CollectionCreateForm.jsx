import React from 'react';
import { Input, Form, Modal } from 'antd';
import PropTypes from 'prop-types';

const CollectionCreateForm = ({ visible, onCancel, onForm, form }) => (
	<Modal
		getContainer={false}
		visible={visible}
		title="Handle a new collection"
		okText="Handle"
		onCancel={onCancel}
		onOk={onForm}
	>
		<Form layout="vertical" form={form}>
			<Form.Item label="Calss Name" name="calssName">
				<Input type="textarea" />
			</Form.Item>
			<Form.Item label="Tag Type" name="tagType">
				<Input type="textarea" />
			</Form.Item>
		</Form>
	</Modal>
);

CollectionCreateForm.propTypes = {
	visible: PropTypes.bool.isRequired,
	onCancel: PropTypes.func.isRequired,
	onForm: PropTypes.func.isRequired,
	form: PropTypes.instanceOf(PropTypes.object).isRequired,
};
export default CollectionCreateForm;
