import {
	HADNDLE_CANCEL,
	GET_DATA,
	HADNDLE_FORM,
	SHOW_MODAL,
	REMOVE_TODO,
} from './frontendTableAction';

const INITIAL_STATE = {
	todos: [],
	visible: false,
	dataIndex: 0,
};

const table = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case GET_DATA: {
			return {
				...state,
				todos: action.payload,
			};
		}
		case HADNDLE_CANCEL: {
			return { ...state, visible: action.payload.visible };
		}
		case HADNDLE_FORM: {
			return {
				...state,
				visible: action.payload.visible,
			};
		}
		case SHOW_MODAL: {
			return {
				...state,
				visible: action.payload.visible,
				dataIndex: action.payload.dataIndex,
			};
		}
		case REMOVE_TODO: {
			return { ...state };
		}
		default:
			return state;
	}
};

export default table;
