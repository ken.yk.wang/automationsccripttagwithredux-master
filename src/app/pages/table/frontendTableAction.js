import axios from 'axios';
import services from '../../config/services';
import { openNotificationError } from '../../utils/notification';

export const HADNDLE_CANCEL = 'HADNDLE_CANCEL';
export const GET_DATA = 'GET_DATA';
export const HADNDLE_FORM = 'HADNDLE_FORM';
export const SHOW_MODAL = 'SHOW_MODAL';
export const REMOVE_TODO = 'REMOVE_TODO';

export const getData = dispatch => {
	const url = `${services.api}`;

	axios
		.get(url)
		.then(response => {
			dispatch({
				type: GET_DATA,
				payload: response.data,
			});
		})
		.catch(error => {
			openNotificationError('error', 'getData', error);
		});
};

export const handleCancel = dispatch => {
	dispatch({
		type: HADNDLE_CANCEL,
		payload: { visible: false },
	});
};

export const handleForm = (dispatch, table, form) => {
	form.validateFields()
		.then(values => {
			const { todos, dataIndex } = table;
			if (dataIndex === -1) {
				const data = {
					calssName: values.calssName,
					tagType: values.tagType,
				};
				const url = `${services.api}`;
				form.resetFields();
				axios
					.post(url, data)
					.then(() => {
						dispatch({
							type: HADNDLE_FORM,
							payload: { visible: false },
						});
						dispatch(getData(dispatch));
					})
					.catch(error => {
						openNotificationError('error', 'add item', error);
					});
			} else {
				const data = {
					calssName: values.calssName,
					tagType: values.tagType,
				};
				const url = services.apiWithId.replace(
					'{orderId}',
					todos[dataIndex].orderId
				);

				form.resetFields();
				axios
					.put(url, data)
					.then(() => {
						dispatch({
							type: HADNDLE_FORM,
							payload: { visible: false },
						});
						dispatch(getData(dispatch));
					})
					.catch(error => {
						openNotificationError('error', 'edit item', error);
					});
			}
		})
		.catch(info => {
			openNotificationError('error', 'handleForm', info);
		});
};

export const showModal = (dispatch, index) => {
	dispatch({
		type: SHOW_MODAL,
		payload: { visible: true, dataIndex: index },
	});
};

export const removeTodo = (dispatch, orderId) => {
	const url = services.apiWithId.replace('{orderId}', orderId);
	axios
		.delete(url)
		.then(() => {
			dispatch({
				type: REMOVE_TODO,
			});
			dispatch(getData(dispatch));
		})
		.catch();
};
