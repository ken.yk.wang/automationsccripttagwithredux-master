export const SHOW_CHATROOM_MODAL = 'SHOW_CHATROOM_MODAL';
export const ON_CLOSE = 'ON_CLOSE';
export const ON_FORM = 'ON_FORM';
export const HANDLE_MESSAGECHANGE = 'HANDLE_MESSAGECHANGE';
export const HANDLE_KEYDOWN = 'HANDLE_KEYDOWN';

export const showChatRoomModal = (dispatch, index) => {
	dispatch({
		type: SHOW_CHATROOM_MODAL,
		payload: { visible: true, currentIndex: index },
	});
};

export const onClose = dispatch => {
	dispatch({
		type: ON_CLOSE,
		payload: { visible: false },
	});
};

export const onForm = dispatch => {
	dispatch({
		type: ON_FORM,
		payload: { visible: false },
	});
};

export const handleMessageChange = (dispatch, event) => {
	dispatch({
		type: HANDLE_MESSAGECHANGE,
		payload: { newMessage: event.target.value },
	});
};

export const handleKeyDown = (dispatch, chatroom, event) => {
	const message = event.target.value;
	const newTime = new Date().toDateString();
	const addMessage = { fromMe: true, text: message, time: newTime };

	if (event.keyCode === 13 && message !== '') {
		const { persons, currentIndex } = chatroom;
		const newPersons = persons;
		newPersons[currentIndex].messages.push(addMessage);

		dispatch({
			type: HANDLE_KEYDOWN,
			payload: { newMessage: '', persons: newPersons },
		});
	}
};
