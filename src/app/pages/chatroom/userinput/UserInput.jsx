import React from 'react';
import PropTypes from 'prop-types';
import './UserInput.scss';

const UserInput = ({ newMessage, messageChange, handleKeyDown }) => (
	<input
		className="new-message"
		value={newMessage}
		onChange={messageChange}
		onKeyDown={handleKeyDown}
	/>
);

UserInput.propTypes = {
	messageChange: PropTypes.func.isRequired,
	handleKeyDown: PropTypes.func.isRequired,
	newMessage: PropTypes.string.isRequired,
};
export default UserInput;
