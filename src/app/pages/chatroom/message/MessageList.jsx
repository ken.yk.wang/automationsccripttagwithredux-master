import React from 'react';
import PropTypes from 'prop-types';
import MessageItem from './MessageItem';

const MessageList = ({ persons, index }) => (
	<div style={{ overflow: 'scroll', height: '100px' }}>
		{persons[index].messages.map(message => (
			<MessageItem fromMe={message.fromMe} text={message.text} />
		))}
	</div>
);

MessageList.propTypes = {
	persons: PropTypes.instanceOf(PropTypes.any).isRequired,
	index: PropTypes.number.isRequired,
};
export default MessageList;
