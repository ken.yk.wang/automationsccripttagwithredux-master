import React from 'react';
import PropTypes from 'prop-types';
import './MessageItem.scss';

const MessageItem = ({ fromMe, text }) => (
	<div
		className={`message-item ${
			fromMe ? 'message-from-me' : 'message-from-other'
		}`}
	>
		<span>{text}</span>
	</div>
);
MessageItem.propTypes = {
	fromMe: PropTypes.bool.isRequired,
	text: PropTypes.string.isRequired,
};
export default MessageItem;
