import React from 'react';
import { List, Button, Avatar } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import ChatRoomModal from './ChatRoomModal';

import {
	showChatRoomModal,
	onClose,
	onForm,
	handleMessageChange,
	handleKeyDown,
} from './ChatRoomAction';

const ChatRoom = () => {
	const chatroom = useSelector(state => state.chatroom);
	const { persons, visible, currentIndex, newMessage } = chatroom;
	const dispatch = useDispatch();

	return (
		<div>
			<List
				dataSource={persons}
				bordered
				renderItem={(item, index) => (
					<List.Item
						key={item.profile.id}
						actions={[
							<Button
								onClick={() =>
									showChatRoomModal(dispatch, index)
								}
								key={`a-${item.profile.id}`}
							>
								Chat Room
							</Button>,
						]}
					>
						<List.Item.Meta
							avatar={<Avatar src={item.profile.avatar} />}
							title={
								<Button href={item.profile.link}>
									{item.profile.name}
								</Button>
							}
							description={item.profile.description}
						/>
					</List.Item>
				)}
			/>
			<ChatRoomModal
				visible={visible}
				currentIndex={currentIndex}
				persons={persons}
				onCancel={() => onClose(dispatch)}
				onForm={() => onForm(dispatch)}
				handleMessageChange={event =>
					handleMessageChange(dispatch, event)
				}
				handleKeyDown={event =>
					handleKeyDown(dispatch, chatroom, event)
				}
				newMessage={newMessage}
			/>
		</div>
	);
};

export default ChatRoom;
