import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'antd';
import MessageList from './message/MessageList';
import UserInput from './userinput/UserInput';

const ChatRoomModal = ({
	visible,
	onCancel,
	onForm,
	currentIndex,
	persons,
	handleMessageChange,
	handleKeyDown,
	newMessage,
}) => (
	<Modal
		className="chatRoomModal"
		style={{
			top: 'auto',
			position: 'absolute',
			bottom: '0px',
			left: 'calc(50% - 260px)',
		}}
		visible={visible}
		title={persons[currentIndex].profile.name}
		onCancel={onCancel}
		onOk={onForm}
		okButtonProps={{ style: { display: 'none' } }}
		cancelButtonProps={{ style: { display: 'none' } }}
	>
		<div className="chatRoomModal__messageList">
			<MessageList persons={persons} index={currentIndex} />
		</div>
		<div className="chatRoomModal__userInput">
			<UserInput
				newMessage={newMessage}
				messageChange={handleMessageChange}
				handleKeyDown={handleKeyDown}
			/>
		</div>
	</Modal>
);

ChatRoomModal.propTypes = {
	visible: PropTypes.bool.isRequired,
	onCancel: PropTypes.func.isRequired,
	onForm: PropTypes.func.isRequired,
	currentIndex: PropTypes.number.isRequired,
	persons: PropTypes.instanceOf(PropTypes.any).isRequired,
	handleMessageChange: PropTypes.func.isRequired,
	handleKeyDown: PropTypes.func.isRequired,
	newMessage: PropTypes.string.isRequired,
};
export default ChatRoomModal;
