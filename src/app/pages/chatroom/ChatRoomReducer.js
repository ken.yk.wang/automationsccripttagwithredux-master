import {
	SHOW_CHATROOM_MODAL,
	ON_CLOSE,
	ON_FORM,
	HANDLE_MESSAGECHANGE,
	HANDLE_KEYDOWN,
} from './ChatRoomAction';

const INITIAL_STATE = {
	persons: [
		{
			profile: {
				id: 1,
				name: 'Lily',
				link: 'https://zh-hant.reactjs.org/docs/hooks-effect.html',
				avatar:
					'https://miro.medium.com/max/676/1*XEgA1TTwXa5AvAdw40GFow.png',
				description: 'Lily Progresser XTech',
			},
			messages: [
				{ fromMe: false, text: '蛤？', time: '12:27am' },
				{ fromMe: false, text: '來來來～', time: '12:27am' },
				{ fromMe: false, text: '靠左邊嗎？', time: '12:27am' },
			],
		},
		{
			profile: {
				id: 2,
				name: 'Ango',
				link: 'https://ant.design/components/list/',
				avatar:
					'https://www.twgreatdaily.com/imgs/image/174/17497297.jpg',
				description: 'Ango Progresser XTech',
			},
			messages: [{ fromMe: false, text: '對啊！', time: '12:27am' }],
		},
		{
			profile: {
				id: 3,
				name: 'Simon',
				link: 'https://ant.design/components/affix/',
				avatar:
					'http://wordpress.bestdaylong.com/wp-content/uploads/2019/07/%E7%8E%89%E5%85%8D%E5%90%83%E6%9C%88%E9%A4%85.jpg',
				description: 'Simon Progresser XTech',
			},
			messages: [{ fromMe: false, text: '你好！', time: '12:27am' }],
		},
	],
	visible: false,
	currentIndex: 0,
	newMessage: '',
};

const chatroom = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case SHOW_CHATROOM_MODAL: {
			return {
				...state,
				visible: action.payload.visible,
				currentIndex: action.payload.currentIndex,
			};
		}
		case ON_CLOSE: {
			return { ...state, visible: action.payload.visible };
		}
		case ON_FORM: {
			return { ...state, visible: action.payload.visible };
		}
		case HANDLE_MESSAGECHANGE: {
			return { ...state, newMessage: action.payload.newMessage };
		}
		case HANDLE_KEYDOWN: {
			return {
				...state,
				newMessage: action.payload.newMessage,
				persons: action.payload.persons,
			};
		}
		default:
			return state;
	}
};

export default chatroom;
