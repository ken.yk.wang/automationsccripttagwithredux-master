import webConfig from './base';

const { domain, contextRoot } = webConfig;

export default {
	getContextRoot: contextRoot,
	getLocale: `${contextRoot}data/`,
	getLocaleEN: `${contextRoot}data/en.json`,
	domain: `${domain}`,
	api: `${domain}/api/customers`, // Your api url
	apiWithId: `${domain}/api/customers/{orderId}`, // Your api url
};
