import React, { useState } from 'react';
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Redirect,
} from 'react-router-dom';
/* You can cache page when page cahnge by import CacheRoute & CacheSwitch */
// import CacheRoute, { CacheSwitch } from 'react-router-cache-route';

import services from './config/services';
import Header from './common/components/Header';

import HomeComponent from './pages/home/Home';
import AboutComponent from './pages/about/About';
import TopicsComponent from './pages/topic/Topics';
import TableComponent from './pages/table/FrontendTable';
import ChatRoomComponent from './pages/chatroom/ChatRoom';

const App = () => {
	const pages = [
		{ path: '/home', name: 'Home', component: HomeComponent },
		{ path: '/about', name: 'About', component: AboutComponent },
		{ path: '/topic', name: 'Topic', component: TopicsComponent },
		{ path: '/table', name: 'Table', component: TableComponent },
	];
	const [show, setShow] = useState(true);

	const controlDisplay = () => {
		setShow(!show);
	};
	return (
		<Router basename={services.getContextRoot}>
			<div className="app">
				<Header pages={pages} />
				<Switch>
					{pages.map((page, index) => (
						<Route
							key={index.toString()}
							exact
							path={page.path}
							component={page.component}
						/>
					))}
					<Redirect to={pages[0].path} />
				</Switch>
				<div
					className="app__circle-wrap"
					onClick={() => controlDisplay()}
				>
					<dev className="app__circle" />
					<div className="app__circle-touch" />
				</div>
				<div
					className="app__chatroom-wrap"
					style={{ display: `${show ? 'none' : 'block'}` }}
					onClick={() => controlDisplay()}
				>
					<div className="app__chatroom">
						<ChatRoomComponent />
					</div>
				</div>
			</div>
		</Router>
	);
};

export default App;
