// css
import './scss/normalize.css';
import './scss/index.scss';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import React from 'react';
import { AppContainer } from 'react-hot-loader';
import ReduxIntlProvider from './app/ReduxIntlProvider';
import App from './app/App';
import multiReducer from './app/multiReducer';

const store = createStore(multiReducer);
const render = Component => {
	ReactDOM.render(
		<Provider store={store}>
			<ReduxIntlProvider>
				<AppContainer>
					<Component />
				</AppContainer>
			</ReduxIntlProvider>
		</Provider>,
		document.getElementById('app')
	);
};

render(App);
if (module.hot) {
	module.hot.accept('./app/App', () => {
		render(App);
	});
}
